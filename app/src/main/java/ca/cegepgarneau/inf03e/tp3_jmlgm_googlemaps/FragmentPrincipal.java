package ca.cegepgarneau.inf03e.tp3_jmlgm_googlemaps;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Jean-Michel on 2018-04-12.
 */

public class FragmentPrincipal extends Fragment

{
    private CommunicateurFVersActivite m_leCommunicateur;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;

        // Création de la vue du fragment.
        view = inflater.inflate(R.layout.test, container,
                false);
        return view;
    }

    // Se produit à chaque fois que l'activité qui contient le fragment est
    // créée (est utile uniquement lorsque retainInstance a été mis à vrai).
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CommunicateurFVersActivite) {
            m_leCommunicateur = (CommunicateurFVersActivite) context;
        }
    }

    // Se produit à chaque fois que l'activité qui contient le fragment est
    // détruite (est utile uniquement lorsque retainInstance a été mis à vrai).

    public void onDetach() {
        super.onDetach();

        // Libération du communicateur.
        m_leCommunicateur = null;
    }
}
