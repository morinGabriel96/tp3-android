package ca.cegepgarneau.inf03e.tp3_jmlgm_googlemaps;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.gms.maps.SupportMapFragment;

public class ActiviteMenu extends AppCompatActivity
{
    private CommunicateurFVersActivite m_leCommunicateur;
    private final String kstrIdAjoutNouvelEndroit = "Fragment ajout nouvel endroit";
    private final String kstrIdRetourCarte = "Fragment retour carte";
    private final String kstrIdModification = "Fragment modification";
    private final String kstrIdSupression = "Fragment supression";

    // NE SERA JAMAIS AFFICHÉ. C'EST COMME LE PARENT DES AUTRES ACTIVITÉS
    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate (savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
        // Création du menu et attachement de celui-ci à l'activité.
        MenuInflater inflater = getMenuInflater ();
        inflater.inflate (R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item)
    {
        // Gestion de chaque élément du menu.
        // Si l'élément n'est pas géré par l'application, le gestionnaire
        // de la classe-mère est appelé.
        switch (item.getItemId ())
        {
            case R.id.mnuAjoutNouvelEndroit:

                getSupportFragmentManager ().beginTransaction ().setCustomAnimations
                        (R.anim.enter_from_left, R.anim.exit_to_right,
                                R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace (android.R.id.content, new FragmentPrincipal())
                        .addToBackStack(kstrIdAjoutNouvelEndroit).commit ();
                return true;
            case R.id.mnuModifierEndroit:
                getSupportFragmentManager ().beginTransaction ().setCustomAnimations
                        (R.anim.enter_from_left, R.anim.exit_to_right,
                                R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace (android.R.id.content, new FragmentPrincipal())
                        .addToBackStack("Ajout nouvel endroit").commit ();
                return true;
            case R.id.mnuSupprimerEndroit:
                getSupportFragmentManager ().beginTransaction ().setCustomAnimations
                        (R.anim.enter_from_left, R.anim.exit_to_right,
                                R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace (android.R.id.content, new FragmentPrincipal())
                        .addToBackStack("Ajout nouvel endroit").commit ();
                return true;
            case R.id.mnuRetourCarte:
                getSupportFragmentManager ().beginTransaction ().setCustomAnimations
                        (R.anim.enter_from_left, R.anim.exit_to_right,
                                R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace (android.R.id.content, new FragmentCarte())
                        .addToBackStack(kstrIdRetourCarte).commit ();
                return true;
            default:
                return super.onOptionsItemSelected (item); // appel le gestionnaire de la classe mère
        }
    }
}
