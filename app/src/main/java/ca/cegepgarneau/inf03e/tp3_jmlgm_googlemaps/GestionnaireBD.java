/******************************************************************************
 Fichier:		GestionnaireBD.java

 Classe:        GestionnaireBD

 Auteurs:		Jean-Michel Longchamps et Gabriel Morin

 Utilité:       Classe qui permet de communiquer avec la base de donnée SQLite
 *****************************************************************************/

package ca.cegepgarneau.inf03e.tp3_jmlgm_googlemaps;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import ca.cegepgarneau.inf03e.tp3_jmlgm_googlemaps.donnees.Endroit;

public class GestionnaireBD extends SQLiteOpenHelper
{
    private static final String kstrNom = "EndroitsClientsFournisseurs.db";
    private static final int kiBDVersion = 1;
    private SQLiteDatabase m_laBd;

    /**************************************************************************
     Constructeur parametré qui permet d'initialiser le contexte
     *************************************************************************/
    public GestionnaireBD(Context context) {
        super(context, kstrNom, null, kiBDVersion);
    }

    /**************************************************************************
     Création de la table et ses champs dans SQLite
     *************************************************************************/
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String strSQLCreationEndroit = "CREATE TABLE Endroits (" +
                "id INTEGER PRIMARY KEY, " +
                "Nom text, " +
                "Adresse text, " +
                "Type text);";

        sqLiteDatabase.execSQL(strSQLCreationEndroit);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int veilleVersion,
                          int nouvelleVersion)
    {
        sqLiteDatabase.execSQL("drop table if exists Endroits;");
        onCreate(sqLiteDatabase);
    }

    // Méthode qui permet d'insérer un nouvel endroit dans la base de donnée
    public void insererEndroit(Endroit nouvelEndroit)
    {
        ContentValues lesValeurs;

        m_laBd = getWritableDatabase ();
        lesValeurs = new ContentValues ();
        lesValeurs.put ("Nom", nouvelEndroit.getNom());
        lesValeurs.put ("Adresse", nouvelEndroit.getAdresse());
        lesValeurs.put ("Type", nouvelEndroit.getType());
        m_laBd.insert ("Endroits", null, lesValeurs);
    }

    // Méthode qui permet de vérifier si la table Endroits est vide
    public boolean listeEstVide()
    {
        String strRequete = "SELECT Count(*) FROM Endroits";
        m_laBd = getReadableDatabase();
        Cursor leCurseur= m_laBd.rawQuery(strRequete, null);
        leCurseur.moveToFirst();
        int iNbArticle = leCurseur.getInt(0);
        boolean bEtatListe = true;

        // La table contient aumoins un enregistrement
        if(iNbArticle > 0)
        {
            bEtatListe = false;
        }

        leCurseur.close();
        return bEtatListe;
    }

    // Méthode qui permet d'obtenir, par défaut, les 20 endroits
    // et de les insérer par défaut dans la base de donnée par la suite
    public void lesEndroits()
    {
        ArrayList<Endroit> lesClientsFournisseurs = new ArrayList<>();

        // Créations des endroits par défauts
        // Clients de la Rive-Sud (3)
        lesClientsFournisseurs.add(new Endroit("Olivier Lachance",
                "1411 Rue des Rocailles, Saint-Romuald, QC G6W 7R4", "c"));
        lesClientsFournisseurs.add(new Endroit("Mélyna Castonguay",
                "273 Rue du Château, Saint-Nicolas, QC G7A 3B8", "c"));
        lesClientsFournisseurs.add(new Endroit("Achile Leblond",
                "85 Rue Édouard Curodeau, Saint-Romuald, QC G6W 7L1", "c"));

        // Clients de la Rive-Nord (7)
        lesClientsFournisseurs.add(new Endroit("Jean-Michel Longchamps",
                "12 Rue Monseigneur-De Laval, Ville de Québec, QC G1R", "c"));
        lesClientsFournisseurs.add(new Endroit("Gabriel Morin",
                "133 Rue Sainte Christine, Ville de Québec, QC G1E 7C4", "c"));
        lesClientsFournisseurs.add(new Endroit("Wyllyam Fortin",
                "350 5e Rue, Ville de Québec, QC G1L", "c"));
        lesClientsFournisseurs.add(new Endroit("Lucie Fontaine",
                "7300 Rue la Marche, Ville de Québec, QC G2K 2A7", "c"));
        lesClientsFournisseurs.add(new Endroit("Bastien Lachance",
                "9220 Rue des Aïeux, Ville de Québec, QC G2K 1J2", "c"));
        lesClientsFournisseurs.add(new Endroit("Jean-Philippe Beaudoin",
                "1457 Rue Bellevue, L'Ancienne-Lorette, QC G2E 3K4", "c"));
        lesClientsFournisseurs.add(new Endroit("Sophia Bilodeau-Belanger",
                "3863 Rue Louise Fiset, Ville de Québec, QC G1X 4N4", "c"));

        // Fournisseurs de la Rive-Sud (3)
        lesClientsFournisseurs.add(new Endroit("Restaurant Normandin",
                "679 Avenue Taniata, Saint-Jean-Chrysostome, QC G6Z 2C1",
                "f"));
        lesClientsFournisseurs.add(new Endroit("RONA",
                "1415 Rue Métivier, Lévis, QC G6V 9M6", "f"));
        lesClientsFournisseurs.add(new Endroit("Moustiquaire MSA",
                "690 chemin Olivier, Lévis, QC G7A 2N2", "f"));

        // Fournisseurs de la Rive-Nord (7)
        lesClientsFournisseurs.add(new Endroit("Tim Hortons",
                "48 Côte du Palais, Ville de Québec, QC G1R 4H8", "f"));
        lesClientsFournisseurs.add(new Endroit("Meubles Léon",
                "2840 Rue Einstein, Sainte-Foy, QC G1X 5H3", "f"));
        lesClientsFournisseurs.add(new Endroit("Quincaillerie St-Jean-Baptiste Inc.",
                "298 Rue D'Aiguillon, Ville de Québec, QC G1R 1L6", "f"));
        lesClientsFournisseurs.add(new Endroit("McDonald's",
                "3900 Boulevard Wilfrid-Hamel, Ville de Québec, QC G1P 2J2",
                "f"));
        lesClientsFournisseurs.add(new Endroit("RONA",
                "2795 Boulevard Père-Lelièvre, Ville de Québec, QC G1P 2X9",
                "f"));
        lesClientsFournisseurs.add(new Endroit("La Poutinerie Québécoise",
                "101 Boulevard D'Anjou, Châteauguay, QC J6J 2R3", "f"));
        lesClientsFournisseurs.add(new Endroit("Artemano Québec",
                "838 Rue Saint-Joseph Est, Ville de Québec, QC G1K 3C9", "f"));

        // Insertion dans la base de donnée SQLite
        for(Endroit endroit : lesClientsFournisseurs)
        {
            insererEndroit(endroit);
        }
    }
}
