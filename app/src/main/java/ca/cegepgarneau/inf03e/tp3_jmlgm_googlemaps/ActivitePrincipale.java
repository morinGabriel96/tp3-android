package ca.cegepgarneau.inf03e.tp3_jmlgm_googlemaps;

import android.support.v4.app.FragmentManager;
import android.os.Bundle;


public class ActivitePrincipale extends ActiviteMenu
{

    GestionnaireBD m_gestionnaireBD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FragmentManager fm;
        fm = getSupportFragmentManager ();

        super.onCreate(savedInstanceState);

        m_gestionnaireBD = new GestionnaireBD(this);

        if(m_gestionnaireBD.listeEstVide())
        {
            m_gestionnaireBD.lesEndroits();
        }

        fm.beginTransaction ().add (android.R.id.content,
                new FragmentCarte(), "ajout")
                .commit ();
    }

/*
    @Override
    public void onBackPressed ()
    {
        FragmentManager fm;
        fm = getSupportFragmentManager ();
        if (fm.getBackStackEntryCount () > 0)
        {
            fm.popBackStackImmediate ();
        }
        else
        {
            super.onBackPressed ();
        }
    }
*/
    /*
    @Override
    public void recevoirAjoutNouvelEndroit() {
        getSupportFragmentManager ().beginTransaction ().setCustomAnimations
                (R.anim.enter_from_left, R.anim.exit_to_right,
                        R.anim.enter_from_right, R.anim.exit_to_left)
                .replace (android.R.id.content, new FragmentAjoutEndroit())
                .addToBackStack("Ajout nouvel endroit").commit ();
    }*/
}
