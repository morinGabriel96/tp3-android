package ca.cegepgarneau.inf03e.tp3_jmlgm_googlemaps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

public class FragmentCarte extends Fragment implements OnMapReadyCallback {

    CommunicateurFVersActivite m_leCommunicateur;
    GoogleMap m_laCarteGoogle;
    private LatLng testLatitude;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FusedLocationProviderClient test = LocationServices.getFusedLocationProviderClient(getActivity());
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        test.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                testLatitude = new LatLng(location.getLatitude(), location.getLongitude());
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View laVue = inflater.inflate(R.layout.fragment_ajouterendroit, container, false);        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        getChildFragmentManager().beginTransaction().replace(R.id.fragmentCarte, mapFragment).commit();
        mapFragment.getMapAsync(this);
        return laVue;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onAttach (Context context)
    {
        super.onAttach (context);
        if (context instanceof CommunicateurFVersActivite)
        {
            m_leCommunicateur = (CommunicateurFVersActivite) context;
        }
    }

    // Se produit à chaque fois que l'activité qui contient le fragment est
    // détruite (est utile uniquement lorsque retainInstance a été mis à vrai).
    @Override
    public void onDetach ()
    {
        super.onDetach ();
        m_leCommunicateur = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        m_laCarteGoogle = googleMap;

        LocationManager locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);

        // Si les permissions ont été accordées, alors on récupère la position
        // de l'utilisateur et on l'affiche
        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            LatLng positionActuelle = testLatitude;

            if (positionActuelle != null) {
                // On ajoute le marqueur (la pin) sur la carte
                m_laCarteGoogle.addMarker(new MarkerOptions()
                        .position(positionActuelle).title("Votre position"));

                // Déplacement de la vue sur la carte sur la position actuelle
                m_laCarteGoogle.moveCamera(CameraUpdateFactory
                        .newLatLng(positionActuelle));
            }
            // Zoom de 15x sur la position actuelle
            m_laCarteGoogle.animateCamera(CameraUpdateFactory.zoomTo(15));
        }
    }
}
