/******************************************************************************
 Fichier:		Endroit.java

 Classe:        Endroit

 Auteurs:		Jean-Michel Longchamps et Gabriel Morin

 Utilité:       Cette classe représente un endroit
 *****************************************************************************/

package ca.cegepgarneau.inf03e.tp3_jmlgm_googlemaps.donnees;

public class Endroit {
    private String m_strNom;
    private String m_strAdresse;
    private String m_cType;

    // Constructeur
    public Endroit(String strNom, String strAdresse, String cType )
    {
        m_cType = cType;
        m_strAdresse = strAdresse;
        m_strNom = strNom;
    }

    // Accesseurs
    public String getNom() {
        return m_strNom;
    }

    public String getAdresse() {
        return m_strAdresse;
    }

    public String getType() {
        return m_cType;
    }
}
