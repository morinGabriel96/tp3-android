/******************************************************************************
 Fichier:		CommunicateurFVersActivite.java

 Auteurs:		Jean-Michel Longchamps et Erick Marticotte

 Utilité:       Cet interface permet la communication entre les fragments et
                l'activité principal
 *****************************************************************************/

package ca.cegepgarneau.inf03e.tp3_jmlgm_googlemaps;

public interface CommunicateurFVersActivite {

    //Pointe vers le fragment Ajouter un nouvel endroit
    public void recevoirAjoutNouvelEndroit();
}
